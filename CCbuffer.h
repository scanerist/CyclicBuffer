#ifndef LABWORK_8_SCANERIST__CCBUFFER_H_
#define LABWORK_8_SCANERIST__CCBUFFER_H_

#include <iostream>

template<typename T, typename allocator = std::allocator<T>>

class CCbuffer {
 private:
  allocator alloc;
  size_t size_;
  int head_;
  int tail_;
  size_t capacity_;
  T *buffer_;
 public:
  class Iterator : public std::iterator<std::random_access_iterator_tag, T> {
   private:
    CCbuffer<T> *buf_;
    unsigned int index_;
   public:
    Iterator() : buf_(nullptr), index_(0) {}
    Iterator(CCbuffer<T> &associated_buf, unsigned int index) : buf_(&associated_buf), index_(index) {}
    explicit Iterator(CCbuffer<T> &associated_buf) : buf_(&associated_buf), index_(0) {}
    Iterator& operator++(){
        index_++;
        return *this;
    }

    Iterator operator++(int){
        Iterator it(*this);
        index_++;
        return it;
    }

    Iterator operator--(int){
        Iterator it(*this);
        index_--;
        return it;
    }
    Iterator operator--(){
        index_--;
        return *this;
    }

    Iterator operator+(const int rhs){
        Iterator it(*this);
        it.index_ += rhs;
        return it;
    }
    Iterator operator-(const int rhs){
        Iterator it(*this);
        it.index_ -= rhs;
        return it;
    }


    bool operator==(const Iterator& rhs){
        return index_ == rhs.index_;
    }
    bool operator!=(const Iterator& rhs){
        return index_ != rhs.index_;
    }
    bool operator>(const Iterator& rhs){
        return index_ > rhs.index_;
    }
    bool operator>=(const Iterator& rhs){
        return index_ >= rhs.index_;
    }
    bool operator<(const Iterator& rhs){
        return index_ < rhs.index_;
    }
    bool operator<=(const Iterator& rhs){
        return index_ <= rhs.index_;
    }
    T& operator[](int index){
        return buf_->buffer_[(index_+index) % buf_->capacity_];
    }
    T& operator*(){
        int act_index = (buf_->head_+ index_) % buf_->capacity_;
        return buf_->buffer_[act_index];
    }
    int operator-(const Iterator& rhs) {
        int delta = index_ - rhs.index_;
        if (delta < 0) {
            delta += buf_->size_;
        }
        return delta;
    }

  };
  class const_iterator : public std::iterator<std::random_access_iterator_tag, const T>{
   private:
    const CCbuffer<T> *buf_;
    unsigned int index_;
   public:
    const_iterator() : buf_(nullptr), index_(0) {}
    const_iterator(const CCbuffer<T> &associated_buf, unsigned int index) : buf_(&associated_buf), index_(index) {}
    explicit const_iterator(const CCbuffer<T> &associated_buf) : buf_(&associated_buf), index_(0) {}
    const_iterator& operator++(){
        index_++;
        return *this;
    }
    const_iterator operator++(int){
        Iterator it(*this);
        index_++;
        return it;
    }
    const_iterator operator--(int){
        Iterator it(*this);
        index_--;
        return it;
    }
    const_iterator operator--(){
        index_--;
        return *this;
    }
    bool operator==(Iterator& rhs) const {
        return index_ == rhs.index_;
    }
    bool operator!=(Iterator& rhs) const{
        return index_ != rhs.index_;
    }
    bool operator>(Iterator& rhs) const{
        return index_ > rhs.index_;
    }
    bool operator>=(Iterator& rhs) const{
        return index_ >= rhs.index_;
    }
    bool operator<(Iterator& rhs) const{
        return index_ < rhs.index_;
    }
    bool operator<=(Iterator& rhs) const{
        return index_ <= rhs.index_;
    }
    const T& operator[](int index){
        return buf_->buffer_[(index_+index) % buf_->capacity_];
    }
    const T& operator*(){
        int act_index = (buf_->head_ + index_) % buf_->capacity_;
        return buf_->buffer[act_index];
    }
  };
  CCbuffer() {
      size_ = 0;
      tail_ = head_ = 0;
      capacity_ = 1;
      buffer_ = alloc.allocate(capacity_);
  }
  explicit CCbuffer(size_t capacity) {
      capacity_ = capacity;
      buffer_ = alloc.allocate(capacity_);
      for (int i = 0; i < capacity_; i++) {
          buffer_[i] = 0;
      }
      tail_ = head_ = 0;
      size_ = 0;
  }
  CCbuffer(size_t capacity, T value_) {
      capacity_ = capacity;
      size_ = 0;
      head_ = tail_ = 0;
      buffer_ = alloc.allocate(capacity_);
      for (int i = 0; i < capacity_; i++) {
          buffer_[i] = value_;
          size_++;
      }
  }
  ~CCbuffer() {
      alloc.deallocate(buffer_, capacity_);
  }
  CCbuffer(const CCbuffer &rhs) {
      tail_ = rhs.tail_;
      capacity_ = rhs.capacity_;
      size_ = rhs.size_;
      head_ = rhs.head_;
      buffer_ = alloc.allocate(rhs.capacity_);
      for (int i = 0; i < size_; i++) {
          buffer_[i] = rhs.buffer_[i];
      }
  }
  CCbuffer(std::initializer_list<T> x){
      buffer_ = alloc.allocate(x.size());
      capacity_ = x.size();
      head_ = tail_ = 0;
      size_ = x.size();
      for(int i = 0; i < x.size(); i++){
          buffer_[i] = *(x.begin() + i);
      }
  }
  CCbuffer &operator=(const CCbuffer &rhs) {
      if (this == &rhs) {
          return *this;
      }
      size_ = rhs.size_;
      capacity_ = rhs.capacity_;
      head_ = rhs.head_;
      tail_ = rhs.tail_;
      alloc.deallocate(buffer_, capacity_);
      buffer_ = alloc.allocate(capacity_);

      for (int i = 0; i < capacity_; i++) {
          buffer_[i] = rhs.buffer_[i];
      }
      return *this;
  }
  bool operator==(CCbuffer &rhs) {
      if (size_ != rhs.size_) {
          return false;
      }
      if (capacity_ != rhs.capacity_) {
          return false;
      }
      for (int i = 0; i < size_; i++) {
          if (buffer_[i] != rhs.buffer_[i]) {
              return false;
          }
      }
      return true;
  }
  bool operator!=(CCbuffer &rhs) {
      if (size_ == rhs.size_) {
          return false;
      }
      if (capacity_ == rhs.capacity_) {
          return false;
      }
      for (int i = 0; i < size_; i++) {
          if (buffer_[i] == rhs.buffer_[i]) {
              return false;
          }
      }
      return true;
  }
  T& operator[](int index){
      return buffer_[(tail_+index) % capacity_];
  }
  void swap(CCbuffer& rhs) {
      std::swap(buffer_, rhs.buffer_);
      std::swap(head_, rhs.head_);
      std::swap(tail_, rhs.tail_);
      std::swap(size_, rhs.size_);
      std::swap(capacity_, rhs.capacity_);

  }
  void push_back(const T& item) {
      if (size_ == capacity_) {
          buffer_[head_] = item;
          head_ = (head_ + 1) % capacity_;
      } else {
          buffer_[tail_] = item;
          tail_ = (tail_ + 1) % capacity_;
          size_++;
      }
  }
  void pop_front() {
      if (size_ > 0) {
          tail_ = (tail_ + 1) % capacity_;
          size_--;
      }
  }
  void clear(){
      for(int i =0; i < size_; i++){
          buffer_[i] = 0;
      }
      size_ = 0;
      head_ = tail_ = 0;
  }
  int size() {
      return size_;
  }
  int max_size() {
      return capacity_;
  }
  bool empty() {
      if (size_ != 0) {
          return false;
      }
      return true;
  }
  Iterator begin(){
      return Iterator(*this, 0);
  }
  Iterator end(){
      return Iterator(*this, size_);
  }
  const_iterator cbegin(){
      return const_iterator(*this, 0);
  }
  const_iterator cend(){
      return const_iterator(*this, size_);
  }

  typedef Iterator it;
  typedef const_iterator const_it;


};

#endif //LABWORK_8_SCANERIST__CCBUFFER_H_
