//
// Created by aliye on 3/15/2023.
//

#ifndef LABWORK_8_SCANERIST__CCBUFFEREXT_H_
#define LABWORK_8_SCANERIST__CCBUFFEREXT_H_

#include <iostream>
template<typename T, typename allocator_ext = std::allocator<T>>

class CCbufferExt {
 private:
  allocator_ext alloc_ext;
  int head_;
  int tail_;
  size_t size_;
  size_t capacity_;
  T *buffer_;
 public:
  class iteratorExt {};
  class const_iteratorExt {};
  CCbufferExt() {
      size_ = 0;
      capacity_ = 1;
      tail_ = head_ = 0;
      buffer_ = alloc_ext.allocate(capacity_);
  }
  CCbufferExt(size_t capacity) {
      size_ = 0;
      capacity_ = capacity;
      tail_ = head_ = 0;
      buffer_ = alloc_ext.allocate(capacity);
  }
  CCbufferExt(size_t capacity, int value) {
      size_ = 0;
      capacity_ = capacity * 2;
      buffer_ = alloc_ext.allocate(capacity_);
      for (int i = 0; i < capacity; i++) {
          buffer_[i] = value;
          size_++;
      }
      head_ = tail_ = 0;
  }
  CCbufferExt(std::initializer_list<T> x){
      buffer_ = alloc_ext.allocate(x.size() * 2);
      capacity_ = x.size();
      head_  = 0;
      tail_ = x.size();
      size_ = x.size();
      for(int i = 0; i < x.size(); i++){
          buffer_[i] = *(x.begin() + i);
      }
  }
  ~CCbufferExt() {
      alloc_ext.deallocate(buffer_, capacity_);
  }
  CCbufferExt &operator=(const CCbufferExt &rhs) {
      if (this == &rhs) {
          return *this;
      }
      size_ = rhs.size_;
      capacity_ = rhs.capacity_;
      head_ = rhs.head_;
      tail_ = rhs.tail_;
      alloc_ext.deallocate(buffer_, capacity_);
      buffer_ = alloc_ext.allocate(capacity_);

      for (int i = 0; i < capacity_; i++) {
          buffer_[i] = rhs.buffer_[i];
      }
      return *this;
  }
  bool operator==(CCbufferExt &rhs) {
      if (size_ != rhs.size_) {
          return false;
      }
      if (capacity_ != rhs.capacity_) {
          return false;
      }
      for (int i = 0; i < size_; i++) {
          if (buffer_[i] != rhs.buffer_[i]) {
              return false;
          }
      }
      return true;
  }
  bool operator!=(CCbufferExt &rhs) {
      if (size_ == rhs.size_) {
          return false;
      }
      if (capacity_ == rhs.capacity_) {
          return false;
      }
      for (int i = 0; i < size_; i++) {
          if (buffer_[i] == rhs.buffer_[i]) {
              return false;
          }
      }
      return true;
  }
  void clear() {
      for (int i = 0; i < size_; i++) {
          buffer_[i] = 0;
      }
      head_ = tail_ = 0;
      size_ = 0;
  }
  int size() {
      return size_;
  }
  int max_size() {
      return capacity_;
  }
  bool empty() {
      if (size_ != 0) {
          return false;
      }
      return true;
  }
  void swap(CCbufferExt& rhs) {
      std::swap(buffer_, rhs.buffer_);
      std::swap(head_, rhs.head_);
      std::swap(tail_, rhs.tail_);
      std::swap(size_, rhs.size_);
      std::swap(capacity_, rhs.capacity_);

  }
  T& operator[](int index){
      return buffer_[(tail_+index) % capacity_];
  }
  class Iterator : public std::iterator<std::random_access_iterator_tag, T> {
   private:
    CCbufferExt<T> *buf_;
    unsigned int index_;
   public:
    Iterator() : buf_(nullptr), index_(0) {}
    Iterator(CCbufferExt<T> &associated_buf, unsigned int index) : buf_(&associated_buf), index_(index) {}
    explicit Iterator(CCbufferExt &associated_buf) : buf_(&associated_buf), index_(0) {}
    Iterator &operator++() {
        index_++;
        return *this;
    }
    Iterator operator++(int) {
        Iterator it(*this);
        index_++;
        return it;
    }
    Iterator operator--(int) {
        Iterator it(*this);
        index_--;
        return it;
    }
    Iterator operator--() {
        index_--;
        return *this;
    }
    Iterator operator+(const int rhs) {
        Iterator it(*this);
        it.index_ += rhs;
        return it;
    }
    Iterator operator-(const int rhs) {
        Iterator it(*this);
        it.index_ -= rhs;
        return it;
    }
    bool operator==(const Iterator &rhs) {
        return index_ == rhs.index_;
    }
    bool operator!=(const Iterator &rhs) {
        return index_ != rhs.index_;
    }
    bool operator>(const Iterator &rhs) {
        return index_ > rhs.index_;
    }
    bool operator>=(const Iterator &rhs) {
        return index_ >= rhs.index_;
    }
    bool operator<(const Iterator &rhs) {
        return index_ < rhs.index_;
    }
    bool operator<=(const Iterator &rhs) {
        return index_ <= rhs.index_;
    }
    T &operator[](int index) {
        return buf_->buffer_[(index_ + index) % buf_->capacity_];
    }
    T &operator*() {
        int act_index = (buf_->head_ + index_) % buf_->capacity_;
        return buf_->buffer_[act_index];
    }
  };
  class const_iterator : public std::iterator<std::random_access_iterator_tag, const T> {
   private:
    const CCbufferExt<T> *buf_;
    unsigned int index_;
   public:
    const_iterator() : buf_(nullptr), index_(0) {}
    const_iterator(const CCbufferExt<T> &associated_buf, unsigned int index) : buf_(&associated_buf), index_(index) {}
    explicit const_iterator(const CCbufferExt<T> &associated_buf) : buf_(&associated_buf), index_(0) {}
    const_iterator &operator++() {
        index_++;
        return *this;
    }
    const_iterator operator++(int) {
        Iterator it(*this);
        index_++;
        return it;
    }
    const_iterator operator--(int) {
        Iterator it(*this);
        index_--;
        return it;
    }
    const_iterator operator--() {
        index_--;
        return *this;
    }
    bool operator==(const const_iterator &rhs) const {
        return index_ == rhs.index_;
    }
    bool operator!=(const const_iterator &rhs) const {
        return index_ != rhs.index_;
    }
    bool operator>(const const_iterator &rhs) const {
        return index_ > rhs.index_;
    }
    bool operator>=(const const_iterator &rhs) const {
        return index_ >= rhs.index_;
    }
    bool operator<(const const_iterator &rhs) const {
        return index_ < rhs.index_;
    }
    bool operator<=(const const_iterator &rhs) const {
        return index_ <= rhs.index_;
    }
    const T &operator[](int index) {
        return buf_->buffer_[(index_ + index) % buf_->capacity_];
    }
    const T &operator*() {
        int act_index = (buf_->head + index_) % buf_->capacity_;
        return buf_->buffer[act_index];
    }
  };
  Iterator begin(){
      return Iterator(*this, 0);
  }
  Iterator end(){
      return Iterator(*this, size_);
  }
  typedef Iterator it;
  void push_back(const T& value_) {
      if (size_ == capacity_) {
          capacity_ *= 2;
          T *tmp = alloc_ext.allocate(capacity_);
          for (int i = 0; i < size_; i++) {
              tmp[i] = buffer_[(tail_ + i) % size_];
          }
          alloc_ext.deallocate(buffer_, capacity_);
          buffer_ = tmp;
          tail_ = 0;
          head_ = size_;

      }
      buffer_[head_] = value_;
      size_++;
      head_ = (head_ + 1) % (capacity_ * 2);
  }
  void pop_front() {
      if (size_ > 0) {
          tail_ = (tail_ + 1) % capacity_;
          size_--;
      }
  }
};

#endif //LABWORK_8_SCANERIST__CCBUFFEREXT_H_
