
#include "CCbufferExt.h"
#include "CCbuffer.h"

#include <gtest/gtest.h>


TEST(CCbuffer, pop_front) {
    CCbuffer<int> buffer(3);
    buffer.push_back(1);
    buffer.push_back(2);
    buffer.push_back(3);
    buffer.pop_front();
    EXPECT_EQ(buffer.max_size(), 3);
    EXPECT_EQ(buffer.size(), 2);
    EXPECT_EQ(buffer[0], 2);
    EXPECT_EQ(buffer[1], 3);
}



TEST(CCbuffer, clear) {
    CCbuffer<int> buffer(3);
    buffer.push_back(1);
    buffer.push_back(2);
    buffer.push_back(3);
    buffer.clear();
    EXPECT_EQ(buffer.size(), 0);
}



TEST(CCbuffer, overwrite) {
    CCbuffer<int> buffer(3);
    buffer.push_back(1);
    buffer.push_back(2);
    buffer.push_back(3);
    buffer.push_back(4);
    EXPECT_EQ(buffer.size(), 3);
    EXPECT_EQ(buffer[0], 4);
    EXPECT_EQ(buffer[1], 2);
    EXPECT_EQ(buffer[2], 3);
}



TEST(CCbuffer, indexing) {
    CCbuffer<int> buffer(3);
    buffer.push_back(1);
    buffer.push_back(2);
    buffer.push_back(3);
    EXPECT_EQ(buffer[0], 1);
    EXPECT_EQ(buffer[1], 2);
    EXPECT_EQ(buffer[2], 3);
}



TEST(CCbuffer, eq_test){
    CCbuffer<int> a(6, 7);
    CCbuffer<int> b = {7, 7, 7, 7, 7,7};
    bool res = (a == b);
    EXPECT_EQ(res, true);
}



TEST(CCbuffer, swap){
    CCbuffer<int> a = {1, 2, 4, 5,6};
    CCbuffer<int> b(7, 6);
    a.swap(b);
    EXPECT_EQ(a[0], 6);
    EXPECT_EQ(a[1], 6);
    EXPECT_EQ(b[0], 1);
    EXPECT_EQ(b[1], 2);
}



TEST(CircularBuffer, iterators) {
    CCbuffer<int> buffer(3);
    buffer.push_back(1);
    buffer.push_back(2);
    buffer.push_back(3);
    int i = 0;
    for (auto it = buffer.begin(); it != buffer.end(); ++it) {
        EXPECT_EQ(*it, ++i);
    }
}



TEST(CCbufferExt, swap){
    CCbufferExt<int> a = {1, 2, 4, 5,6};
    CCbufferExt<int> b(7, 6);
    a.swap(b);
    EXPECT_EQ(a[0], 6);
    EXPECT_EQ(a[1], 6);
    EXPECT_EQ(b[0], 1);
    EXPECT_EQ(b[1], 2);
}



TEST(CircularBufferExt, max_size) {
    CCbufferExt<int> buffer(3);
    buffer.push_back(1);
    buffer.push_back(2);
    buffer.push_back(3);
    EXPECT_EQ(buffer.max_size(), 3);
    buffer.push_back(4);
    EXPECT_EQ(buffer.max_size(), 6);
    EXPECT_EQ(buffer[0], 1);
    EXPECT_EQ(buffer[1], 2);
    EXPECT_EQ(buffer[2], 3);
    EXPECT_EQ(buffer[3], 4);
}



TEST(CircularBufferExt, push_back) {
    CCbufferExt<int> buffer;
    buffer.push_back(1);
    buffer.push_back(2);
    buffer.push_back(3);
    buffer.push_back(4);
    EXPECT_EQ(buffer.max_size(), 4);
    EXPECT_EQ(buffer.size(), 4);
    EXPECT_EQ(buffer[0], 1);
    EXPECT_EQ(buffer[1], 2);
    EXPECT_EQ(buffer[2], 3);
    EXPECT_EQ(buffer[3], 4);
}



TEST(CircularBufferExt,pop_front) {
    CCbufferExt<int> buffer;
    buffer.push_back(1);
    buffer.push_back(2);
    buffer.push_back(3);
    buffer.pop_front();
    EXPECT_EQ(buffer.max_size(), 4);
    EXPECT_EQ(buffer.size(), 2);
    EXPECT_EQ(buffer[0], 2);
    EXPECT_EQ(buffer[1], 3);
}



TEST(CircularBufferExt, clear) {
    CCbufferExt<int> buffer;
    buffer.push_back(1);
    buffer.push_back(2);
    buffer.push_back(3);
    buffer.push_back(4);
    buffer.clear();
    EXPECT_EQ(buffer.size(), 0);
}



TEST(CircularBufferExt, overwrite) {
    CCbufferExt<int> buffer;
    buffer.push_back(1);
    buffer.push_back(2);
    buffer.push_back(3);
    buffer.push_back(4);
    buffer.push_back(5);
    EXPECT_EQ(buffer.size(), 5);
    EXPECT_EQ(buffer.max_size(), 8);
    EXPECT_EQ(buffer[0], 1);
    EXPECT_EQ(buffer[1], 2);
    EXPECT_EQ(buffer[2], 3);
    EXPECT_EQ(buffer[3], 4);
    EXPECT_EQ(buffer[4], 5);
}



TEST(CircularBufferExt, indexing) {
    CCbufferExt<int> buffer;
    buffer.push_back(1);
    buffer.push_back(2);
    buffer.push_back(3);
    buffer.push_back(4);
    EXPECT_EQ(buffer[0], 1);
    EXPECT_EQ(buffer[1], 2);
    EXPECT_EQ(buffer[2], 3);
    EXPECT_EQ(buffer[3], 4);
}



TEST(CircularBufferExt, iterators) {
    CCbufferExt<int> buffer;
    buffer.push_back(1);
    buffer.push_back(2);
    buffer.push_back(3);
    buffer.push_back(4);
    int i = 0;
    for (auto it = buffer.begin(); it != buffer.end(); ++it) {
        EXPECT_EQ(*it, ++i);
    }
    int j = 0;
    for (auto & it : buffer) {
        EXPECT_EQ(it, ++j);
    }
}




